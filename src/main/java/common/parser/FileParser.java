package common.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang.StringUtils.split;

public class FileParser {
    public List<String[]> rows = new ArrayList<String[]>();
    private String separator = ",";

    public FileParser(String path, String separator) throws IOException {
        if (separator != null)
            this.separator = separator;

        BufferedReader reader = new BufferedReader(new FileReader(path));
        try {
            String line = reader.readLine();
            while (line != null) {
                rows.add(split(line, this.separator));
                line = reader.readLine();
            }
        } finally {
            reader.close();
        }
    }
}
