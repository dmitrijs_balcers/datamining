package naive.bayers.method.common.funciton;

import common.parser.FileParser;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;
import static java.math.RoundingMode.HALF_UP;

public class Functions extends FileParser {
    private final List<String[]> meetsResult = new ArrayList<String[]>();
    public int total;

    public Functions(String path, int i1, int i) throws IOException {
        super(path, null);

        for (String[] row : rows) {
            if (row[i1].equals(valueOf(i))) {
                total++;
                meetsResult.add(row);
            }
        }
    }

    public BigDecimal posteriorProbability(int[] classifiers, int[] values) {
        checkArrays(classifiers, values);

        BigDecimal result = priorProbability();
        for (int i = 0; i < classifiers.length; i++)
            result = result.multiply(conditionalProbability(classifierTotal(classifiers[i], values[i]), total));

        return result.setScale(4, HALF_UP);
    }

    private static void checkArrays(int[] classifiers, int[] values) {
        if (classifiers.length != values.length) {
            throw new RuntimeException("Array lengths should be equal");
        }
    }

    public BigDecimal priorProbability() {
        return new BigDecimal(total).setScale(8).divide(new BigDecimal(rows.size()), HALF_UP);
    }

    public int classifierTotal(int classifier, int value) {
        int total = 0;
        for (String[] row : meetsResult)
            if (row[classifier].equals(valueOf(value)))
                total++;
        return total;
    }

    public static BigDecimal conditionalProbability(int condition, int result) {
        return new BigDecimal(condition).setScale(8).divide(new BigDecimal(result), HALF_UP);
    }
}
