package naive.bayers.method.trains;

public class Enums {
    public enum Result {
        ON_TIME,
        LATE,
        VERY_LATE,
        CANCELLED
    }

    public enum Rain {
        NONE,
        SLIGHT,
        HEAVY
    }

    public enum Wind {
        NONE,
        HIGH,
        NORMAL
    }

    public enum Season {
        SPRING,
        WINTER,
        AUTUMN,
        SUMMER
    }

    public enum Day {
        WEEKDAY,
        SATURDAY,
        HOLIDAY,
        SUNDAY
    }
}
