package naive.bayers.method.trains;

public class ClassificationObject {
    ClassificationObject(Enums.Day day, Enums.Season season, Enums.Wind wind, Enums.Rain rain, Enums.Result result) {
        this.day = day;
        this.season = season;
        this.wind = wind;
        this.rain = rain;
        this.result = result;
    }

    public Enums.Day day;
    public Enums.Season season;
    public Enums.Wind wind;
    public Enums.Rain rain;
    public Enums.Result result;
}
