package naive.bayers.method.trains.total;

import naive.bayers.method.trains.ClassificationObject;
import naive.bayers.method.trains.TrainResource;

import java.util.List;

public abstract class TotalCounter extends TrainResource {
    private List<ClassificationObject> meetsResult;

    protected TotalCounter(List<ClassificationObject> meetsResult) {
        this.meetsResult = meetsResult;
    }

    public int getTotal() {
        int total = 0;
        for (ClassificationObject object : meetsResult) {
            if (equal(object))
                total++;
        }
        return total;
    }

    public abstract boolean equal(ClassificationObject object);
}
