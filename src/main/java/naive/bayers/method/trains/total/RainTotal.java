package naive.bayers.method.trains.total;

import naive.bayers.method.trains.ClassificationObject;
import naive.bayers.method.trains.Enums;

import java.util.List;

public class RainTotal extends TotalCounter {
    private Enums.Rain rain;

    public RainTotal(List<ClassificationObject> meetsResult, Enums.Rain rain) {
        super(meetsResult);
        this.rain = rain;
    }

    @Override
    public boolean equal(ClassificationObject object) {
        return object.rain == rain;
    }
}
