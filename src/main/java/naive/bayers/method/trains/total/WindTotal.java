package naive.bayers.method.trains.total;

import naive.bayers.method.trains.ClassificationObject;
import naive.bayers.method.trains.Enums;

import java.util.List;

public class WindTotal extends TotalCounter {
    private Enums.Wind wind;

    public WindTotal(List<ClassificationObject> meetsResult, Enums.Wind wind) {
        super(meetsResult);
        this.wind = wind;
    }

    @Override
    public boolean equal(ClassificationObject object) {
        return object.wind == wind;
    }
}
