package naive.bayers.method.trains.total;

import naive.bayers.method.trains.ClassificationObject;

import java.util.List;

import static naive.bayers.method.trains.Enums.Season;

public class SeasonTotal extends TotalCounter {
    private Season season;

    public SeasonTotal(List<ClassificationObject> meetsResult, Season season) {
        super(meetsResult);
        this.season = season;
    }

    @Override
    public boolean equal(ClassificationObject object) {
        return object.season == season;
    }
}
