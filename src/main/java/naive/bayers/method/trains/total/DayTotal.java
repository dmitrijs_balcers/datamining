package naive.bayers.method.trains.total;

import naive.bayers.method.trains.ClassificationObject;
import naive.bayers.method.trains.Enums;

import java.util.List;

public class DayTotal extends TotalCounter {
    private Enums.Day day;

    public DayTotal(List<ClassificationObject> meetsResult, Enums.Day day) {
        super(meetsResult);
        this.day = day;
    }

    @Override
    public boolean equal(ClassificationObject object) {
        return object.day == day;
    }
}
