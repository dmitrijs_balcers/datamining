package naive.bayers.method.trains;

import naive.bayers.method.trains.total.DayTotal;
import naive.bayers.method.trains.total.RainTotal;
import naive.bayers.method.trains.total.SeasonTotal;
import naive.bayers.method.trains.total.WindTotal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.math.RoundingMode.HALF_UP;
import static naive.bayers.method.trains.Enums.*;

@Deprecated
public class FunctionsTrain extends TrainResource {
    private List<ClassificationObject> meetsResult = new ArrayList<ClassificationObject>();
    int total = 0;

    public FunctionsTrain(Enums.Result result) {
        for (ClassificationObject object : objects) {
            if (object.result == result) {
                meetsResult.add(object);
                total++;
            }
        }
    }

    public BigDecimal posteriorProbability(Day day, Season season, Wind wind, Rain rain) {
        BigDecimal dayCondProb = conditionalProbability(getTotal(day), total);
        BigDecimal seasonCondProb = conditionalProbability(getTotal(season), total);
        BigDecimal windCondProb = conditionalProbability(getTotal(wind), total);
        BigDecimal rainCondProb = conditionalProbability(getTotal(rain), total);

        return priorProbability()
                .multiply(windCondProb)
                .multiply(dayCondProb)
                .multiply(seasonCondProb)
                .multiply(rainCondProb).setScale(4, HALF_UP);
    }

    public int getTotal(Day day) {
        return new DayTotal(meetsResult, day).getTotal();
    }

    public int getTotal(Season season) {
        return new SeasonTotal(meetsResult, season).getTotal();
    }

    public int getTotal(Wind wind) {
        return new WindTotal(meetsResult, wind).getTotal();
    }

    public int getTotal(Rain rain) {
        return new RainTotal(meetsResult, rain).getTotal();
    }

    public static BigDecimal conditionalProbability(int condition, int result) {
        return new BigDecimal(condition).setScale(2).divide(new BigDecimal(result), HALF_UP);
    }

    public BigDecimal priorProbability() {
        return new BigDecimal(total).setScale(2).divide(new BigDecimal(objects.size()));
    }

    public int getTotal() {
        return total;
    }
}
