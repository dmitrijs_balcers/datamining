package naive.bayers.method.trains;

import java.util.ArrayList;
import java.util.List;

import static naive.bayers.method.trains.Enums.Day.*;
import static naive.bayers.method.trains.Enums.Rain.HEAVY;
import static naive.bayers.method.trains.Enums.Rain.SLIGHT;
import static naive.bayers.method.trains.Enums.Result.*;
import static naive.bayers.method.trains.Enums.Season.*;
import static naive.bayers.method.trains.Enums.Wind.*;

public class TrainResource {

    static List<ClassificationObject> objects = objects();

    public static List<ClassificationObject> objects() {
        List<ClassificationObject> objects = new ArrayList<ClassificationObject>();
        objects.add(new ClassificationObject(WEEKDAY, SPRING, NONE, Enums.Rain.NONE, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, WINTER, NONE, SLIGHT, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, WINTER, NONE, SLIGHT, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, WINTER, HIGH, HEAVY, LATE));

        objects.add(new ClassificationObject(SATURDAY, SUMMER, NORMAL, Enums.Rain.NONE, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, AUTUMN, NORMAL, Enums.Rain.NONE, VERY_LATE));
        objects.add(new ClassificationObject(HOLIDAY, SUMMER, HIGH, SLIGHT, ON_TIME));
        objects.add(new ClassificationObject(SUNDAY, SUMMER, NORMAL, Enums.Rain.NONE, ON_TIME));

        objects.add(new ClassificationObject(WEEKDAY, WINTER, HIGH, HEAVY, VERY_LATE));
        objects.add(new ClassificationObject(WEEKDAY, SUMMER, NONE, SLIGHT, ON_TIME));
        objects.add(new ClassificationObject(SATURDAY, SPRING, HIGH, HEAVY, CANCELLED));
        objects.add(new ClassificationObject(WEEKDAY, SUMMER, HIGH, SLIGHT, ON_TIME));

        objects.add(new ClassificationObject(SATURDAY, WINTER, NORMAL, Enums.Rain.NONE, LATE));
        objects.add(new ClassificationObject(WEEKDAY, SUMMER, HIGH, Enums.Rain.NONE, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, WINTER, NORMAL, HEAVY, VERY_LATE));
        objects.add(new ClassificationObject(SATURDAY, AUTUMN, HIGH, SLIGHT, ON_TIME));

        objects.add(new ClassificationObject(WEEKDAY, AUTUMN, NONE, HEAVY, ON_TIME));
        objects.add(new ClassificationObject(HOLIDAY, SPRING, NORMAL, SLIGHT, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, SPRING, NORMAL, Enums.Rain.NONE, ON_TIME));
        objects.add(new ClassificationObject(WEEKDAY, SPRING, NORMAL, SLIGHT, ON_TIME));
        return objects;
    }
}
