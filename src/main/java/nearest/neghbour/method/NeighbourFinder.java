package nearest.neghbour.method;

import common.parser.FileParser;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.sort;
import static nearest.neghbour.method.DistanceMeter.euclideanDistance;

public class NeighbourFinder extends FileParser {

    List<Neighbour> neighbours = new ArrayList<Neighbour>();

    /**
     * @param path to the file
     * @param poi  point of interest
     */
    public NeighbourFinder(String path, String separator, String[] poi) throws IOException {
        super(path, separator);
        for (String[] row : rows)
            neighbours.add(new Neighbour(euclideanDistance(new String[][]{poi, row}), row));
    }

    /**
     * @param count number of nearest neighbours to be returned
     * @return
     */
    public List<Neighbour> getNeighbours(int count) {
        sort(neighbours);
        return getNeighboursPage(count);
    }

    private List<Neighbour> getNeighboursPage(int count) {
        List<Neighbour> neighboursPage = new ArrayList<Neighbour>();
        for (int i = 0; i < count; i++)
            neighboursPage.add(neighbours.get(i));
        return neighboursPage;
    }

    class Neighbour implements Comparable<Neighbour> {

        private String[] point;
        private BigDecimal distance;

        public Neighbour(BigDecimal distance, String[] point) {
            this.distance = distance;
            this.point = point;
        }

        public String[] getPoint() {
            return point;
        }

        public BigDecimal getDistance() {
            return distance;
        }

        @Override
        public int compareTo(Neighbour o) {
            return distance.compareTo(o.getDistance());
        }

        @Override
        public String toString() {
            return "Neighbour{" +
                    "point=" + Arrays.toString(point) +
                    ", distance=" + distance +
                    '}';
        }
    }
}
