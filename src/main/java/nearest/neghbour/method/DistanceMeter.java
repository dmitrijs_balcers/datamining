package nearest.neghbour.method;

import java.math.BigDecimal;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;

public class DistanceMeter {


    /**
     * @param points
     * @return sqrt(pow(a1-b1, 2)+(a2-b1,2)+...+pow(an-bn,2))
     */
    public static BigDecimal euclideanDistance(String[][] points) {
        BigDecimal result = ZERO.setScale(8);
        for (int column = 0; column <= points[0].length - 1; column++) {
            result = result.add(pow2(columnDifference(points, column)));
        }
        return squareRoot(result);
    }

    private static BigDecimal columnDifference(String[][] points, int column) {
        BigDecimal columnDifference = new BigDecimal(points[0][column]);
        for (int row = 1; row <= points.length - 1; row++) {
            columnDifference = columnDifference.subtract(new BigDecimal(points[row][column]));
        }
        return columnDifference;
    }

    private static BigDecimal pow2(BigDecimal columnDifference) {
        return new BigDecimal(pow(columnDifference.doubleValue(), 2));
    }

    private static BigDecimal squareRoot(BigDecimal result) {
        return new BigDecimal(sqrt(result.doubleValue())).setScale(4, HALF_UP);
    }

}
