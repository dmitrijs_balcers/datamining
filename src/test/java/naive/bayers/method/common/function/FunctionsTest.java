package naive.bayers.method.common.function;

import naive.bayers.method.common.funciton.Functions;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static naive.bayers.method.common.funciton.Functions.conditionalProbability;
import static org.fest.assertions.Assertions.assertThat;
import static org.testng.Assert.assertEquals;

/**
 * Day:       1=WEEKDAY, 	2=SATURDAY, 3=HOLIDAY, 		4=SUNDAY
 * Season:    1=WINTER, 	2=SPRING,	3=AUTUMN,		4=SUMMER
 * Wind:	    1=NONE,		2=HIGH,		3=NORMAL
 * Rain:	    1=NONE,		2=SLIGHT,	3=HEAVY
 * Class:	    1=ON_TIME,	2=LATE,		3=VERY_LATE,	4=CANCELLED
 */
@Test
public class FunctionsTest {
    Functions functions = getFunctions();

    public void shouldFind14OnTimeClasses() throws IOException {
        assertThat(functions.total).isEqualTo(14);
    }

    public void findClassifierTotal() {
        assertThat(functions.classifierTotal(1, 1)).isEqualTo(2);
    }

    public void testConditionalProbabilityOfTrainArrivingInSummerOnTime() {
        assertEquals(conditionalProbability(functions.classifierTotal(1, 4), functions.total), new BigDecimal("0.42857143"));
    }

    public void testPriorProbability() {
        assertEquals(functions.priorProbability(), new BigDecimal("0.70000000"));
    }

    private Functions getFunctions() {
        try {
            return new Functions("src/test/resources/file.txt", 4, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
