package naive.bayers.method.common.function.posterior.probability;

import naive.bayers.method.common.funciton.Functions;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;


/**
 * Day:       1=WEEKDAY, 	2=SATURDAY, 3=HOLIDAY, 		4=SUNDAY
 * Season:    1=WINTER, 	2=SPRING,	3=AUTUMN,		4=SUMMER
 * Wind:	    1=NONE,		2=HIGH,		3=NORMAL
 * Rain:	    1=NONE,		2=SLIGHT,	3=HEAVY
 * Class:	    1=ON_TIME,	2=LATE,		3=VERY_LATE,	4=CANCELLED
 */
@Test
public class TestPosteriorProbability {
    public void testOnTimeWithGivenConditions() {
        Functions functions = getFunctions(4, 1);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 1, 2, 3}, new int[]{1, 1, 2, 3});
        assertEquals(posteriorProbability, new BigDecimal("0.0013"));
    }

    public void testLateWithGivenConditions() {
        Functions functions = getFunctions(4, 2);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 1, 2, 3}, new int[]{1, 1, 2, 3});
        assertEquals(posteriorProbability, new BigDecimal("0.0125"));
    }

    public void testVeryLateWithGivenConditions() {
        Functions functions = getFunctions(4, 3);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 1, 2, 3}, new int[]{1, 1, 2, 3});
        assertEquals(posteriorProbability, new BigDecimal("0.0222"));
    }

    public void testCancelledWithGivenConditions() {
        Functions functions = getFunctions(4, 4);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 1, 2, 3}, new int[]{1, 1, 2, 3});
        assertEquals(posteriorProbability, new BigDecimal("0.0000"));
    }

    private Functions getFunctions(int classId, int valueId) {
        try {
            return new Functions("src/test/resources/file.txt", classId, valueId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
