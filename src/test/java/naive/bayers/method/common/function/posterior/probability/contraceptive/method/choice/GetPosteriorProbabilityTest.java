package naive.bayers.method.common.function.posterior.probability.contraceptive.method.choice;

import naive.bayers.method.common.funciton.Functions;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;

/**
 * Attribute Information:
 * <p/>
 * 1. Wife's age (numerical)
 * 2. Wife's education (categorical) 1=low, 2, 3, 4=high
 * 3. Husband's education (categorical) 1=low, 2, 3, 4=high
 * 4. Number of children ever born (numerical)
 * 5. Wife's religion (binary) 0=Non-Islam, 1=Islam
 * 6. Wife's now working? (binary) 0=Yes, 1=No
 * 7. Husband's occupation (categorical) 1, 2, 3, 4
 * 8. Standard-of-living index (categorical) 1=low, 2, 3, 4=high
 * 9. Media exposure (binary) 0=Good, 1=Not good
 * 10. Contraceptive method used (class attribute) 1=No-use, 2=Long-term, 3=Short-term
 */

@Test
public class GetPosteriorProbabilityTest {

    public void that38yoWomanWithLowEducationWillNotChooseContraception() {
        Functions functions = getFunctions(9, 1);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 1}, new int[]{38, 1});
        assertEquals(posteriorProbability, new BigDecimal("0.0022"));
    }

    public void that30yoWomanWithHighEducationWillNotChooseLongTermContraception() {
        Functions functions = getFunctions(9, 2);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 1}, new int[]{30, 4});
        assertEquals(posteriorProbability, new BigDecimal("0.0046"));
    }

    public void that37yoIslamicWomanWithHighEducationWillNotChooseShortTermContraception() {
        Functions functions = getFunctions(9, 3);
        BigDecimal posteriorProbability = functions.posteriorProbability(new int[]{0, 5, 1}, new int[]{37, 1, 4});
        assertEquals(posteriorProbability, new BigDecimal("0.0041"));
    }

    private Functions getFunctions(int classId, int valueId) {
        try {
            return new Functions("src/test/resources/contraception-method-choice", classId, valueId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
