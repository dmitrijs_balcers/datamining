package naive.bayers.method.trains;

import org.testng.annotations.Test;

import java.math.BigDecimal;

import static naive.bayers.method.trains.Enums.Day.WEEKDAY;
import static naive.bayers.method.trains.Enums.Rain.HEAVY;
import static naive.bayers.method.trains.Enums.Result.ON_TIME;
import static naive.bayers.method.trains.Enums.Season.SUMMER;
import static naive.bayers.method.trains.Enums.Season.WINTER;
import static naive.bayers.method.trains.Enums.Wind.HIGH;
import static naive.bayers.method.trains.FunctionsTrain.conditionalProbability;
import static org.testng.Assert.assertEquals;

@Test
@Deprecated
public class TestOnTime {
    FunctionsTrain functions = new FunctionsTrain(ON_TIME);

    public void testGetTotalOnTimeResults() {
        assertEquals(functions.getTotal(), 14);
    }

    public void testGetTotalWinterOnTime() {
        assertEquals(functions.getTotal(WINTER), 2);
    }

    public void testConditionalProbabilityOfTrainArrivingInWinterOnTime() {
        assertEquals(conditionalProbability(functions.getTotal(WINTER), functions.getTotal()), new BigDecimal("0.14"));
    }

    public void testConditionalProbabilityOfTrainArrivingInSummerOnTime() {
        assertEquals(conditionalProbability(functions.getTotal(SUMMER), functions.getTotal()), new BigDecimal("0.43"));
    }

    public void testConditionalProbabilityOfTrainArrivingInWeekdayOnTime() {
        assertEquals(conditionalProbability(functions.getTotal(WEEKDAY), functions.getTotal()), new BigDecimal("0.64"));
    }

    public void testConditionalProbabilityOfTrainArrivingInHighWindOnTime() {
        assertEquals(conditionalProbability(functions.getTotal(HIGH), functions.getTotal()), new BigDecimal("0.29"));
    }

    public void testConditionalProbabilityOfTrainArrivingInHeavyRainOnTime() {
        assertEquals(conditionalProbability(functions.getTotal(HEAVY), functions.getTotal()), new BigDecimal("0.07"));
    }

    public void testPriorProbability() {
        assertEquals(functions.priorProbability(), new BigDecimal("0.70"));
    }
}
