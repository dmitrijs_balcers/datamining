package naive.bayers.method.trains.posteriorProbability;

import naive.bayers.method.trains.FunctionsTrain;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static naive.bayers.method.trains.Enums.Day.WEEKDAY;
import static naive.bayers.method.trains.Enums.Rain.HEAVY;
import static naive.bayers.method.trains.Enums.Result.*;
import static naive.bayers.method.trains.Enums.Season.WINTER;
import static naive.bayers.method.trains.Enums.Wind.HIGH;
import static org.testng.Assert.assertEquals;

@Test
@Deprecated
public class TestPosteriorProbability {
    public void testOnTimeWithGivenConditions() {
        BigDecimal posteriorProbability = new FunctionsTrain(ON_TIME).posteriorProbability(WEEKDAY, WINTER, HIGH, HEAVY);
        assertEquals(posteriorProbability, new BigDecimal("0.0013"));
    }

    public void testLateWithGivenConditions() {
        BigDecimal posteriorProbability = new FunctionsTrain(LATE).posteriorProbability(WEEKDAY, WINTER, HIGH, HEAVY);
        assertEquals(posteriorProbability, new BigDecimal("0.0125"));
    }

    public void testVeryLateWithGivenConditions() {
        BigDecimal posteriorProbability = new FunctionsTrain(VERY_LATE).posteriorProbability(WEEKDAY, WINTER, HIGH, HEAVY);
        assertEquals(posteriorProbability, new BigDecimal("0.0222"));
    }

    public void testCancelledWithGivenConditions() {
        BigDecimal posteriorProbability = new FunctionsTrain(CANCELLED).posteriorProbability(WEEKDAY, WINTER, HIGH, HEAVY);
        assertEquals(posteriorProbability, new BigDecimal("0.0000"));
    }
}
