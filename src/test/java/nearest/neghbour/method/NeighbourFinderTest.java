package nearest.neghbour.method;

import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;

@Test
public class NeighbourFinderTest {
    public void shouldFindNearestNeighbours() throws IOException {
        List<NeighbourFinder.Neighbour> neighbours = new NeighbourFinder("src/test/resources/nearestNeighbour/testData.txt",
                ",", new String[]{"1", "2", "1"})
                .getNeighbours(5);
        assertThat(neighbours).isNotNull().isNotEmpty().hasSize(5);

        printNeighbours(neighbours);
    }

    public void shouldFindNearestNeighboursUsingRealData() throws IOException {
        List<NeighbourFinder.Neighbour> neighbours = new NeighbourFinder("src/test/resources/nearestNeighbour/fertility_Diagnosis.txt",
                ",", new String[]{"0.33", "0.69", "0", "1", "1", "0", "0.8", "0", "0.88", "1"})
                .getNeighbours(10);
        assertThat(neighbours).isNotNull().isNotEmpty().hasSize(10);

        printNeighbours(neighbours);

        System.out.println("Grouped Results1: ");
        System.out.println(groupResults(neighbours, 9).toString());

        neighbours = new NeighbourFinder("src/test/resources/nearestNeighbour/fertility_Diagnosis.txt",
                ",", new String[]{"0.33", "0.75", "1", "0", "1", "0", "0.8", "-1", "0.44", "0"})
                .getNeighbours(10);
        assertThat(neighbours).isNotNull().isNotEmpty().hasSize(10);

        printNeighbours(neighbours);

        System.out.println("Grouped Results2: ");
        System.out.println(groupResults(neighbours, 9).toString());
    }

    private Map<String, Integer> groupResults(List<NeighbourFinder.Neighbour> neighbours, int pointOutputId) {
        Map<String, Integer> groupedNeighbours = new HashMap<String, Integer>();
        for (NeighbourFinder.Neighbour neighbour : neighbours) {
            String outputResult = neighbour.getPoint()[pointOutputId];
            if (!groupedNeighbours.containsKey(outputResult)) {
                groupedNeighbours.put(outputResult, 1);
            } else {
                groupedNeighbours.put(outputResult, groupedNeighbours.get(outputResult) + 1);
            }
        }
        return groupedNeighbours;
    }

    private static void printNeighbours(List<NeighbourFinder.Neighbour> neighbours) {
        for (NeighbourFinder.Neighbour neighbour : neighbours)
            System.out.println(neighbour.toString());
    }


}
