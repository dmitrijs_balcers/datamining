package nearest.neghbour.method;

import org.testng.annotations.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static nearest.neghbour.method.DistanceMeter.euclideanDistance;
import static org.fest.assertions.Assertions.assertThat;

@Test
public class DistanceMeasureMethodTest {
    //root((0-0)^2) = 0
    public void distanceShouldBeEqualTo0() {
        assertThat(euclideanDistance(new String[][]{{"0"}, {"0"}})).isEqualTo(ZERO.setScale(4));
    }

    //root((1-0)^2) = 1
    public void distanceShouldBeEqualTo1() {
        assertThat(euclideanDistance(new String[][]{{"1"}, {"0"}})).isEqualTo(ONE.setScale(4));
    }

    //root((3-1)^2) = 2
    public void distanceShouldBeEqualTo2() {
        assertThat(euclideanDistance(new String[][]{{"3"}, {"1"}})).isEqualTo(new BigDecimal("2.0000"));
    }

    //root(((2-0)^2) + ((3-1))^2) = 2,82
    public void distanceShouldBeEqualTo2c82() {
        assertThat(euclideanDistance(new String[][]{{"2", "3"}, {"0", "1"}})).isEqualTo(new BigDecimal("2.8284"));
    }

    public void distanceToItselfShouldBeEqual() {
        assertThat(euclideanDistance(new String[][]{{"2", "3"}, {"2", "3"}})).isEqualTo(ZERO.setScale(4));
    }

    public void theDistanceFromAtoB_shouldBeTheSameAsBtoA() {
        assertThat(euclideanDistance(new String[][]{{"2", "3"}, {"2", "4"}}))
                .isEqualTo(euclideanDistance(new String[][]{{"2", "4"}, {"2", "3"}}));
    }

    public void triangleIntegrity() {
        assertThat(euclideanDistance(new String[][]{{"0", "0"}, {"100", "100"}}))
                .isEqualTo(euclideanDistance(new String[][]{{"0", "0"}, {"50", "50"}})
                        .add(euclideanDistance(new String[][]{{"50", "50"}, {"100", "100"}})));
    }

}
