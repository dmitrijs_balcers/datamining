package common.parser;

import org.fest.assertions.Assertions;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

import static org.apache.commons.lang.StringUtils.split;

@Test
public class FileParserTest {
    public void shouldParse20Rows() throws IOException {
        List<String[]> rows = new FileParser("src/test/resources/file.txt", null).rows;
        Assertions.assertThat(rows).hasSize(20);
    }

    public void shouldSplitStringIntoArray() {
        String string = "1,2,3,4,5";
        String[] array = split(string, ",");
        Assertions.assertThat(array).hasSize(5).isEqualTo(new String[]{"1", "2", "3", "4", "5"});
    }
}
